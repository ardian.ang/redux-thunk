import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { getItems } from "./Redux/action";

import "./App.css";

function App() {
  const { items, loading, error } = useSelector((state) => state);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getItems());
  }, [dispatch]);

  return (
    <div className="App">
      <form>
        <input placeholder="masukkan title" name="title"></input>
        <button type="submit" value="Submit">
          Add Title
        </button>
      </form>

      {loading && !error ? (
        <div>Loading.........</div>
      ) : (
        items.map((item) => (
          <div key={item.id}>
            <h2>Title: {item.title}</h2>
            {/* <p>{item.body}</p> */}
          </div>
        ))
      )}
    </div>
  );
}

export default App;
