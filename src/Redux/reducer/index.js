const initialState = {
  items: [],
  loading: false,
  error: "",
};

export default function itemReducer(state = initialState, action) {
  switch (action.type) {
    case "item/get-pending":
      return {
        ...state,
        loading: true,
      };
    case "item/get-success":
      return {
        ...state,
        loading: false,
        items: action.payload,
      };
    case "item/get-failed":
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    default:
      return state;
  }
}
