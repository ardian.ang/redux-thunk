import axios from "axios";

export const getItems = () => {
    return (dispatch, getstate) => {
      dispatch({ type: "item/get-pending" });
      //api call
      axios
        .get("https://jsonplaceholder.typicode.com/posts")
        .then((response) => {
            // console.log(response);
          dispatch({ type: "item/get-success", payload: response.data });
        })
        .catch((error) => {
          dispatch({ type: "item/get-failed", payload: error });
        });
    };
  };